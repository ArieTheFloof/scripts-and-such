#!/usr/bin/env python
from os import system as sys
conkyItems = ['sys', 'info', 'network']

def start():
    for x in conkyItems:
        sys('conky -c ./' + x + '.conf')

start()