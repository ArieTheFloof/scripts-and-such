output = ''
for num in range(1, 10):
    num = str(num)
    output += f'${{top_mem name {num}}}${{goto 170}}${{top_mem pid {num}}}${{goto 220}}${{top_mem cpu {num}}}${{goto 285}}${{top_mem mem_res {num}}}\n'
print(output)