from yaml import safe_load

def networkGeneral():
    with open('secrets.yml', 'r') as stream:
        secrets = safe_load(stream)

    nwInterface = secrets.get('network_interface')

    output = ''
    output += '${color0}Total down: $color${totaldown ' + nwInterface + '} ${color0}Total up: $color${totalup '+nwInterface+'}\n'

    print(output)

networkGeneral()
