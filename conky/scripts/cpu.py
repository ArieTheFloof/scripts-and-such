from os import popen as popen

output = ''
cores = popen('cat /proc/cpuinfo | grep cores | uniq |  awk \'{print $4}\'').read()
frequencies = popen('cat /proc/cpuinfo | grep MHz | awk \'{print $4}\'').read().split()

threads = int(len(frequencies))
threadsPerCore = int(threads / int(cores))

index = 0
for core in range(0, int(cores)):
    output += f'${{color0}}CPU Core {core}: $color {frequencies[index]}MHz ${{color0}} Load: $color${{cpu cpu{core}}}%\n'
    output += f'${{cpubar cpu{core} 8,340}}\n'
    index += threadsPerCore

print(output)