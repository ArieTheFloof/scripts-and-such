from yaml import safe_load
from os import popen as popen
from sys import argv

with open('secrets.yml', 'r') as stream:
    secrets = safe_load(stream)


def cputemp():
    cpuSensor = secrets.get('cpu_temp').split(' ')
    output = '${color0}CPU Temperature(s):\n'
    for i in cpuSensor:
        cpuTemperature = popen(f'sensors | grep \'{i}:\' | awk \'{{print substr($2,2)}}\'').read()

        
        output += f'${{color0}} {i} $color ${{goto 285}}{cpuTemperature}'
    print(output)


def gputemp():
    gpuSensor = secrets.get('gpu_temp').split(' ')
    output = '${color0}GPU Temperature(s):\n'
    for i in gpuSensor:
        gpuTemperature = popen(f'sensors | grep \'{i}:\' | awk \'{{print substr($2,2)}}\'').read()
        output += f'${{color0}} {i} $color ${{goto 285}}{gpuTemperature}'
    print(output)



argument = argv[1]

if argument == "-cpu":
    cputemp()

if argument == "-gpu":
    gputemp()
