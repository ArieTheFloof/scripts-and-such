from yaml import safe_load
from os import popen as popen

def networkStatic():
    with open('secrets.yml', 'r') as stream:
        secrets = safe_load(stream)

    nwInterface = secrets.get('network_interface')
    nameservers = str(popen('cat /etc/resolv.conf | grep ^nameserver | awk \'{print $2}\'').read().strip())

    output = ''
    output += '${color0}Local Ip: $color${addr ' + nwInterface + '}\n'
    output += '${color0}DNS: $color ' + nameservers + '\n'

    print(output)

networkStatic()
