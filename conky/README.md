My simple and trimmed down conky setup based on https://github.com/alexbel/conky re-written in python.

To use for yourself, put your network interface in the `secrets.yml` file (found by running `ip addr` in the terminal) and then simply run 
```
python run.py
```

![](images/conkyPythonSidebar.png)
![](images/conkyPythonDesktop.png)

