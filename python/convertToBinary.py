# Converts decimal to binary
#import sys


def toBin(decimal, highest2Power, binary):
    """ Calculates the binary value of a positive integer

    Input: Positive integer decimal, highest power of 2, and a list
    Output: Prints binary value for given integer

    For Example:
    >>> toBin(20, 4, [])
    10100
    """
    remainder = decimal
    for n in range(0, highest2Power + 1):
        if remainder - 2 ** (highest2Power - n) >= 0:
           remainder = remainder - 2 ** (highest2Power - n)
           binary.append(str(1))
        else:
           binary.append(str(0))
        n+=1
    print(f"Binary : {''.join(binary)}\n")


def highestPower(base, decimal, n):
    """ Determines the highest n power a base can go to before being larger than a decimal integer

    Input: three integers, base, decimal, and n
    Output: the largest value of n such that the n power of base is >= decimal
    
    For example:
    >>> highestPower(2, 20, 0)
    4
    """
    if base ** (n + 1) > decimal:
        return n
    return(highestPower(base, decimal, n + 1))


def grabInput(prompt):
    """ Asks user to input a positive integer based on the prompt given
    """
    print(prompt)
    decimal = input("Decimal: ")
    while True:
        try:
            decimal = int(decimal)
            if decimal >= 0:
                return decimal
            else:
                print(f"\n{(decimal)} is a negative integer. Please use a positive one")
        except:
            print(f"\n{decimal} is not an integer. Please try again")
            return grabInput(prompt)


if __name__ == "__main__":
    # Allows user to set recursion limit, probably not needed
    #while True:
    #    limit = grabInput("Please input the desired recursionlimit (1000 is usually more than enough)")
    #    sys.setrecursionlimit(limit)
    #    break
    while True:
        decimal = grabInput("Please input the desired positive integer to be converted into binary")
        toBin(decimal, highestPower(2, decimal, 0), [])
