# Energy Python

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

reading = "read"
date = "date"
sample_size = "D"

# Read csv file
first = pd.read_csv(
    "energy.csv",
     parse_dates = [date],
     index_col = [date],
     dayfirst = True
)

# Read csv file
second = pd.read_csv(
    "newenergy.csv",
     parse_dates = [date],
     index_col = [date],
     dayfirst = True
)


# Initalise DataFrames
energyUsage = pd.DataFrame(first,columns = [reading])
newenergyUsage = pd.DataFrame(second,columns = [reading])

# Prints a summary
print(energyUsage.tail())
print(newenergyUsage.head())

frames = [energyUsage, newenergyUsage]
result = pd.concat(frames)

idx = np.unique(result.index.values, return_index = True)[1]
result = result.iloc[idx]

result.to_csv('concat_energy.csv')