""" Energy Python
A small script I use to graph my home energy usage.
It gives you some stats about your usage, and graphs it.

22-09-2022 Allison

Example Output:

Most Recent Energy Usage
               read
date               
2022-09-16  38.4045
2022-09-18  30.2916
2022-09-20  18.8299 

Maximum: On the 18 July, 2022 a total of 44.6602kWh was used
Minimum: On the 12 March, 2022 a total of 3.5545kWh was used
Mean: An average of 27.9003kWh was used per 2D
Total: A total of 3096.9333kWh was used since 12 February, 2022
"""

import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from datetime import date

# Variables
today = date.today()
datetimeformat = '%d %B, %Y' # Default DD MonthName, YYYY
reading = "read"
date = "date"
sample_size = "2D"
sample_size_multiple = int(''.join(filter(str.isdigit, sample_size)))
sample_size_format = sample_size.replace(str(sample_size_multiple), "")
smooth_sample_size = str(sample_size_multiple * 2) + sample_size_format

# Matplot variables
colormap = "jet"
plt.style.use(['ggplot', 'dark_background'])
mpl.rcParams['figure.figsize'] = [8.0, 6.0]
mpl.rcParams['date.converter'] = 'concise'
mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['figure.dpi'] = 200
plt.rcParams['axes.xmargin'] = 0

# Read csv file
data = pd.read_csv(
    "energy.csv",
     parse_dates = [date],
     index_col = [date],
     dayfirst = True
)

# Initalise DataFrames
energy = pd.DataFrame(data,columns = [reading]).resample(sample_size).sum()

# Prints a summary of recent energy usage
print(f"The sample size is {sample_size}\n")
print("Most Recent Energy Usage")
print(energy.tail(3),"\n")

# Prints stats
average = energy[reading].mean().round(4) 
maximum = [energy[reading].max().round(4), energy[reading].idxmax().strftime(datetimeformat)]
minimum = [energy[reading].min().round(4), energy[reading].idxmin().strftime(datetimeformat)]
total = energy[reading].sum().round(4)
start = energy.reset_index()[date][0].strftime(datetimeformat)

print(f"Maximum: On the {maximum[1]} a total of {maximum[0]}kWh was used")
print(f"Minimum: On the {minimum[1]} a total of {minimum[0]}kWh was used")
print(f"Mean: An average of {average}kWh was used per {sample_size}")
print(f"Total: A total of {total}kWh was used since {start}")

def plot_save(name):    
    plt.xlabel(
        "Date",
        fontsize = 12,
        color = "black"
    )
    
    plt.ylabel(
        "Energy Consumption kWh",
        fontsize = 12,
        color = "black"
    )
    plt.ylim(0)
    plt.savefig(f"{today}-{name}")
    plt.show()
    plt.close()

# Plot Energy Usage, change these to customize the graph
energy.plot(
    title = f"Energy Usage (kWh) per {sample_size}",
    color = 'red',
    linewidth = 2
)

plot_save("line_graph.png")

# Scatter plot
energy.reset_index().plot.scatter(
    'date', 
    'read', 
    c='read', 
    cmap=colormap, 
    title = f"Energy Usage (kWh) per {sample_size}"
)
plot_save("scatter_graph.png")

# Scatter plot with interpolated data to look more 'smooth'
energy.resample('30min').mean().interpolate().reset_index().plot.scatter(
    'date', 
    'read', 
    c='read', 
    cmap=colormap,
    s = 3,
    title = f"Energy Usage (kWh) per {sample_size} interpolated"
)
plot_save("scatter_graph_interpolated.png")

# Resample to 2*sample_size with the average. Add extra rows and interpolate
energy_interp = energy.resample(f"{smooth_sample_size}").mean().resample('30min').mean().interpolate()
energy_interp.reset_index().plot.scatter(
    'date', 
    'read', 
    c='read', 
    cmap=colormap,
    s = 3,
    title = f"Energy Usage (kWh) per {smooth_sample_size} interpolated & smoothed"
)
plot_save("scatter_graph_interpolated_smooth.png")

