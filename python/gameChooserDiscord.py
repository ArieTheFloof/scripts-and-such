"""
A very simple discord bot that helps you choose a game if you're indecisive.
"""

prefix = ">>"
games = ["Stellaris", "Minecraft", "Rimworld"] # <<- Replace these with your games
token = "" # <-- Put your token here

from discord.ext import commands
import discord
from random import randint

bot = commands.Bot(command_prefix=">>")
bot.remove_command("help")
status = discord.Game(f"Type {prefix}game")
last_game = None
print(f"Bot's prefix is {prefix}")


# Game Choice Command
@bot.command()
async def game(ctx, *args):
    """
    Incredibly crude way of selecting a random game that is different to the
    previous one selected using global variables and 'while True', but it's
    simple and works.
    """
    global last_game
    while True:
        selected_game = games[randint(0, len(games) - 1)]
        if selected_game is not last_game:
            last_game = selected_game
            break
    print(f"{selected_game} has been selected!")
    await ctx.channel.send(selected_game)


# Prints information once bot connects to discord
@bot.event
async def on_ready():
    print("Logged in as")
    print(f"Name: {bot.user.name}")
    print(f"Id: {bot.user.id}")
    await bot.change_presence(status=discord.Status.online, activity=status)


# Tries to start bot
try:
    bot.run(token)
except Exception as error:
    print("Token unreadable!")
    print(error)
