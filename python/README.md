# Python

---

Some scripts which I've used to do things, usually basic maths or commands. Expect documentation to be all over the place at times.

So far in my **python** scripts folder, I have:

---

### [convertToBinary.py](https://gitlab.com/P-90-For-Retail/scripts-and-such/-/blob/master/python/convertToBinary.py)
Converts decimal numbers into binary numbers. Useful to have if you're lazy and have no internet access.
### [convertToDecimal.py](https://gitlab.com/P-90-For-Retail/scripts-and-such/-/blob/master/python/convertToDecimal.py)
Converts binary numbers into decimal numbers. Again, useful to have in some situations.
### [periodicImageUploaderDiscord.py](https://gitlab.com/P-90-For-Retail/scripts-and-such/-/blob/master/python/periodicImageUploaderDiscord.py)
Uploads images periodically to a discord channel. (Requested by a friend)
### [gameChooserDiscord.py](https://gitlab.com/P-90-For-Retail/scripts-and-such/-/blob/master/python/gameChooserDiscord.py)
A little discord bot to help me choose a game to play.
### [energy_graph/](https://gitlab.com/P-90-For-Retail/scripts-and-such/-/blob/master/python/energy_graph)
Outputs graphs of my energy usage from my providor's csv file. 